import pandas as pd
import matplotlib.pyplot as plt
import time
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import matplotlib.pyplot as plt
from datetime import date
from datetime import datetime
from sklearn.model_selection import train_test_split 
from sklearn import datasets, linear_model, metrics 

def createrecwindow():
    #accessory recommendation
    data_init=pd.read_csv("accessoryparameters.csv")

    dates=data_init['PURCHASE DATE']
    days=[]
    todays=  date.today()
    for i in dates:
        date_object = datetime.strptime(i, '%Y,%m,%d').date()
    
        num=todays-date_object
    
        days.append(num.days)


    data_init['DEVICE AGE']=days
    del data_init['PURCHASE DATE']
    Y=data_init['RECOMMEND'][:].values
    X=data_init.drop(['RECOMMEND'],axis=1).values
#print(X)
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, 
                                                    random_state=1) 
  
# create linear regression object 
    reg = linear_model.LinearRegression() 
  
# train the model using the training sets 
    reg.fit(X_train, y_train) 
  
# regression coefficients 
    print('Coefficients: \n', reg.coef_) 
    y_pred = reg.predict(X_test) 


    print('Variance score: {}'.format(reg.score(X_test, y_test))) 
    print(y_test)
    y_pred1=y_pred.astype(int)
    y_pred1
    
    temp=pd.read_csv("prodtest.csv")
    securitytag=e1.get()
    y=temp['SERVICE TAG']
    y.dropna()
    for i in range (0,y.shape[0]):  
        if(float(securitytag)== float(y[i])):
            index=i;
            break
    x1=[]
    x1.append(temp['ACCESSORY 1'][index])
    x1.append(temp['ACCESSORY 2'][index])
    x1.append(temp['ACCESSORY 3'][index])
    x1.append(temp['WARRANTY STATUS'][index])
    dates=temp['PURCHASE DATE'][index]
    dates_object=datetime.strptime(dates,'%Y,%m,%d').date()
    todays=date.today()
    number=todays-dates_object
    x1.append(number.days)
    x1=np.array(x1)
    print(x1)
    ypred1=np.dot(x1,reg.coef_)
    if(ypred1<=0):
        acv=0
        ypred1=acv
        print(ypred1)
    else:
        ypred1=round(ypred1)
        acv=ypred1
        print(acv)
    
    
    """y1=reg.predict(x1);
    y2=y1.astype(int)
    print(y1)"""
    #antivirusrecommendation
    data_init1=pd.read_csv("antivirusparameters.csv")

    del data_init1['ANTIVIRUS PERIOD']

    dates1=data_init1['PURCHASE DATE']
    days1=[]
    today1=date.today()
    for i1 in dates1:
        date_object1 = datetime.strptime(i1, '%Y,%m,%d').date()
        date_object
        num1=today1-date_object
        days1.append(num1.days)
        
    data_init1['DEVICE AGE']=days1
        
    del data_init1['PURCHASE DATE']

    Y=data_init1['ANTIVIRUS NUMBER'][:].values
    X=data_init1.drop(['ANTIVIRUS NUMBER'],axis=1).values
    #print(X)
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, 
                                                    random_state=100) 
  
    # create linear regression object 
    reg1 = linear_model.LinearRegression() 
  
    # train the model using the training sets 
    reg1.fit(X_train, y_train) 
  
    # regression coefficients 
    print('Coefficients: \n', reg1.coef_) 

    """y_pred1 = np.dot(X_test,reg.coef_)"""


    print('Variance score: {}'.format(reg.score(X_test, y_test))) 
    print(y_test)
    y_pred1=y_pred1.astype(int)
    y_pred1
    
    x2=[]
    x2.append(temp['STATUS OF ANTIVIRUS'][INDEX])
    x2.append(temp['ANTIVIRUS PRICE'][index])
    dates=temp['PURCHASE DATE'][index]
    dates_object=datetime.strptime(dates,'%Y,%m,%d').date()
    todays=date.today()
    number=todays-dates_object
    x2.append(number.days)
    x2=np.array(x2);
    ypred2=np.dot(x2,reg.coef_)
    if(ypred2<=0):
        avv=0
        ypred2=avv
        print(ypred2)
    else:
        ypred2=round(ypred2)
        avv=ypred2
        print(avv)
    

def createrrorwindow():
    messagebox.showerror("ERROR!","ERROR 404: Service tag doesn't exist")
    
def helpwindow():
    helpwindow=Tk()
    helpwindow.title('DELL SERVICES');
    helpwindow.configure(background='black') 
    helpwindow.geometry("700x700")
    u=Label(helpwindow, text='PLEASE CONTACT DELL SERVICES TO UPDATE YOUR PROFILE')
    u.place(width=350,x=190,y=350)
    q=Label(helpwindow, text='DELL CUSTOMER CARE : 8142369143')
    q.place(width=350,x=190,y=370)
    w=Label(helpwindow, text='OR EMAIL US AT dellservices@lauda.com')
    w.place(width=350,x=190,y=390)
def createinfowindow():
    index=0
    infowindow=Tk()
    infowindow.title('DELL SERVICES');
    infowindow.configure(background='black') 
    infowindow.geometry("700x700")
    infos=pd.read_csv("prodtest.csv")
    securitytags=e1.get()
    y=infos['SERVICE TAG']
    y.dropna()
    for i in range (0,y.shape[0]):  
        if(float(securitytags)== float(y[i])):
            index=i;
            break
        
    service=Label(infowindow,text="SERVICE TAG :"+str(int(infos['SERVICE TAG'][index])))
    service.place(width=260,x=250,y=150)
    name=Label(infowindow,text="OWNER NAME  :"+infos['OWNER NAME'][index])
    name.place(width=260,x=250,y=170)
    model=Label(infowindow,text="MODEL     :"+infos['MODEL'][index])
    model.place(width=260,x=250,y=190)
    date=Label(infowindow,text="PURCHASE DATE :"+infos['PURCHASE DATE'][index])
    date.place(width=260,x=250,y=210)
    warranty=Label(infowindow,text="WARRANTY PERIOD :"+str(int(infos['WARRANTY PERIOD'][index]))+" years")
    warranty.place(width=260,x=250,y=230)
    a=Label(infowindow, text='ACCESSORIES BOUGHT AT TIME OF PURCHASE',fg='green')
    a.place(width=260,x=250,y=250)
    if(int(infos['ACCESSORY 1'][index])==1):
        mouse=Label(infowindow,text='MOUSE WAS BOUGHT ')
        mouse.place(width=260,x=250,y=270)
    else:
        mouse=Label(infowindow,text='MOUSE WAS NOT BOUGHT')
        mouse.place(width=260,x=250,y=270)
    if(int(infos['ACCESSORY 2'][index])==1):
        key=Label(infowindow,text='KEYBOARD WAS BOUGHT')
        key.place(width=260,x=250,y=290)
    else:
        key=Label(infowindow,text='KEYBOARD WAS NOT BOUGHT')
        key.place(width=260,x=250,y=290)  
        
    p=Label(infowindow, text='THE DETAILS MENTIONED ARE',fg='black',bg='cyan')
    p.place(width=200,x=250,y=520)
    button = Button(infowindow, text='INACCURATE',fg='black',bg='cyan',width=25,command=helpwindow) 
    button.place(x=360,y=550)
    button = Button(infowindow, text='ACCURATE',fg='black',bg='cyan',width=25,command=createrecwindow) 
    button.place(x=150,y=550)
    infowindow.mainloop()
       
def checkvalidity():
    i=0
    l=0
    info=pd.read_csv("prodtest.csv")
    securitytag=e1.get()
    x=info['SERVICE TAG']
    x.dropna()
    """print (x)
    while(x[i]!=0):
        l=l+1;
    print (l)"""
    for i in range (0,x.shape[0]):  
        if(float(securitytag)== float(x[i])):
           createinfowindow();  
           break
    else:
        createrrorwindow();
    
       
    

from tkinter import *
window = Tk()
window.title("ksdasndsnadn")

bg_image = PhotoImage(file ="dell4.png")
x = Label (window, image = bg_image)
x.grid(row = 0, column = 0)

window.geometry("1000x562") 
l=Label(window, text='SERVICE TAG',fg='black',bg='cyan')
l.place(width=100,x=300,y=400)
e1 = Entry(window)
e1.place(width=190,x=410,y=400)
button = Button(window, text='Enter',fg='black',bg='cyan',width=25,command=checkvalidity) 
button.place(x=410,y=450)
"""info=pd.read_csv("prodtest.csv")
securitytag=e1.get()
x=info['SERVICE TAG']
for i in range(0,x.shape[0]):
    print (x[i]);"""
r.mainloop()