# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 10:46:23 2019

@author: santosh meduri
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import datetime
from datetime import datetime


# In[264]:


train = pd.read_csv("warrantyparameters.csv")
train.head()


# In[265]:


from datetime import date
train_df = pd.DataFrame(train)
dates = train_df['PURCHASE DATE']
time_gap = [0] * dates.shape[0]
for i in range (0,dates.shape[0]):
    date_object = datetime.strptime(dates[i], '%Y,%m,%d').date()
    y = date.today() - date_object
    time_gap[i] = y.days/365
train_df['DEVICE AGE'] = time_gap
train_df = train_df.drop("PURCHASE DATE", axis=1)
train_df.head()


# In[266]:


import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score


# In[267]:


xx = train_df
xx = xx.drop("WARRANTY PERIOD", axis=1)
xx = xx.drop("MANUFACTURE DATE", axis=1)
xx = xx.drop("WARRANTY MODEL NUMBER", axis=1)
xx['PRICE_MODEL'] = xx['PRICE_MODEL'].astype(float,errors='ignore')
yy = train_df['WARRANTY MODEL NUMBER']


# In[268]:


#train_x = xx.iloc[:23,:]
#test_x = xx.iloc[23:,:]
#train_y = yy.iloc[:23,]
#test_y = yy.iloc[23:,]
from sklearn.model_selection import train_test_split 
train_x, test_x, train_y, test_y = train_test_split(xx, yy, test_size=0.367, random_state=1)
print(train_x)
print(test_x)


# In[269]:


regr = linear_model.LinearRegression(copy_X=True, fit_intercept=True, n_jobs=None,
         normalize=True)
regr.fit(train_x, train_y)


# In[270]:


pred = regr.predict(test_x)
pred_y.append(pred)
print(pred_y)
print('Coefficients: \n', regr.coef_)
print("Mean squared error: %.2f"
      % mean_squared_error(test_y, pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(test_y, pred))


# In[273]:


pred_y = pred.tolist()
test_y = test_y
print(pred_y)
print(test_y)
correct = 0
for i in range(0,len(pred_y)):
    pred_y[i] = int(pred_y[i])
    if pred_y[i] > 5:
        pred_y[i] = 5


# In[110]:


from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[251]:


from sklearn import preprocessing
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
polynomial_features= PolynomialFeatures(degree=6)
x_poly = polynomial_features.fit_transform(train_x)
polynomial_features.fit(x_poly,train_y)
model = LinearRegression()
model.fit(x_poly, train_y)
y_poly_pred = model.predict(x_poly)
y_poly_pred.tolist()


# In[252]:


for i in range(0,len(y_poly_pred)):
    y_poly_pred[i] = int(y_poly_pred[i])
    if y_poly_pred[i] > 5:
        y_poly_pred[i] = 5
print('Coefficients: \n', regr.coef_)
print("Mean squared error: %.2f" % mean_squared_error(train_y, y_poly_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(train_y, y_poly_pred))


# In[261]:


x_poly_test = polynomial_features.fit_transform(test_x)
yhat = model.predict(x_poly_test)
yhat.tolist()


# In[274]:


print('Coefficients: \n', regr.coef_)
print("Mean squared error: %.2f" % mean_squared_error(test_y, yhat))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(test_y, yhat))


# In[279]:


temp = pd.read_csv("prodtest.csv")
temp.iloc[2, :5]